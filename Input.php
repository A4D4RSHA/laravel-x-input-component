<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\View\View;

class Input extends Component
{
    public string $clean_field = "";

    public function __construct(
        public string  $name,
        public string  $label,
        public ?string $id = null,
        public string  $type = "text",
        public mixed   $current = "",
        public bool    $required = false,
        public bool    $old = true,
        public bool    $error = true,
        public bool    $margin = true,
        public bool    $autofocus = false,
        public string  $key = "id",
        public string  $value = "name",
        public bool    $empty = true,
        public mixed   $options = [],
        public bool    $multiple = false,
        public bool    $checked = false,
        public int     $rows = 2,
        public bool    $switch = false,
    )
    {
        if (empty($this->id)) {
            $this->id = $this->name;
        }
    }

    public function render(): View
    {
        // Remove brackets of $key
        $this->clean_field = str_replace('[]', '', $this->id);

        return view('components.bootstrap.input');
    }
}
