<div @class([
        'mb-3' => $margin,
        'form-check' => in_array(strtolower($type), ['checkbox', 'radio']),
        'form-switch form-switch-lg' => strtolower($type) === "checkbox" && $switch,
    ])>
    @if (!empty($label) && (empty($type) || !in_array(strtolower($type), ['checkbox', 'radio'])))
        <label class="form-label" for="{{ $clean_field }}">
            {!! $label !!}
            @if ($required ?? false)
                <x-bootstrap.required/>
            @endif
        </label>
    @endif

    @if (!empty($type) && strtolower($type) == 'textarea')

        <textarea
                id="{{ $clean_field }}"
                name="{{ $name }}"
                {{$attributes}}
                @class(['form-control', 'is-invalid' => $errors->has($name)])
                @if($autofocus) autofocus @endif
                @if ($required ?? false) required @endif
                rows="{{ $rows ?? 2 }}"
        >{{ $old ?? true ? old($name, $current ?? null) : '' }}</textarea>

    @elseif(!empty($type) && strtolower($type) == 'checkbox')

        <input
                type="checkbox"
                name="{{ $name }}"
                id="{{ $clean_field }}"
                value="{{ $value ?? 1 }}"
                {{$attributes}}
                class="form-check-input"
                @checked(old($name, $checked))
        >

        <label for="{{ $clean_field }}" class="form-check-label mb-1">
            <span class="ms-2">
                {!! $label !!}
                @if($required)
                    <x-bootstrap.required/>
                @endif
            </span>
        </label>

    @elseif(!empty($type) && strtolower($type) == 'radio')

        <input
                type="radio"
                name="{{ $name }}"
                id="{{ $clean_field }}_{{ Str::slug($value) }}"
                value="{{ $current }}"
                {{$attributes}}
                class="form-check-input"
                @checked(old($name, $checked))
        >
        <label for="{{ $clean_field }}_{{ Str::slug($value) }}" class="form-check-label mb-1">
            <span class="ml-2">
                {!! $label !!}
                @if($required)
                    <x-bootstrap.required/>
                @endif
            </span>
        </label>

    @elseif(!empty($type) && strtolower($type) == 'select')

        <select name="{{ $name }}" id="{{ $clean_field }}"
                @class(['form-select', 'is-invalid' => $errors->has($name)])
                {{$attributes}}
                @if ($autofocus) autofocus @endif
                @if ($multiple) multiple="multiple" @endif>
            @if ($empty)
                <option value="">Choose One...</option>
            @endif

            @if (empty($key))
                @foreach ($options as $item)
                    <option value="{{ $item }}"
                            @if ($multiple) @if (in_array($item, $current)) selected @endif @else
                        @if ($current == $item) selected @endif @endif
                    >{{ $item }}</option>
                @endforeach
            @else
                @foreach ($options as $item)
                    <option value="{{ $item[$key] }}"
                            @if ($multiple) @if (in_array($item[$key], $current)) selected @endif @else
                        @if ($current == $item[$key]) selected @endif @endif
                    >{{ $item[$value] }}</option>
                @endforeach
            @endif
        </select>

    @else

        <div class="input-group">
            <input id="{{ $clean_field }}" type="{{ $type ?? 'text' }}" name="{{ $name }}"
                   @class(['form-control', 'is-invalid' => $errors->has($name)])
                   @if ($old ?? true) value="{{ old($name, $current ?? null) }}" @endif
                   @if ($required ?? false) required @endif
                   {{$attributes}}
                   @if ($autofocus ?? false) autofocus @endif />
            @if (empty($label) && ($required ?? false))
                <div class="input-group-text bg-light">
                    <x-bootstrap.required/>
                </div>
            @endif
        </div>

    @endif

    @if ($error ?? true)
        @error($clean_field)
        <div class="text-danger mt-2">
            <small>{{ $message }}</small>
        </div>
        @enderror
    @endif
</div>
