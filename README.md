# Laravel `<x-input />`

This custom component provides basic support for Bootstrap Input Methods, including: `<input>`, `<textarea>` and `<select>`.

This component is fine-tuned for best use with Bootstrap 5. For the Tailwind CSS version, you can use the [tailwind branch](https://gitlab.com/A4D4RSHA/laravel-x-input-component/-/tree/tailwind?ref_type=heads)'s code.

This is a work in progress. Expect it to be released as a separate package for laravel with update support.
For now, you need to manually install and update it.

## Table of Content

- [Supported Fields](#all_supported_fields)
- [Installation & Update](#installation)
- [Examples](#examples)
  - [Simple Text Input (Create Form)](#example_simple_text)
  - [Simple Text Input (Edit Form)](#example_simple_text_edit)
  - [Simple Text Input (Required)](#example_simple_text_required)
  - [Simple Checkbox](#example_simple_checkbox)
  - [Simple Checkbox as Switch](#example_simple_checkbox_switch)
  - [Simple Select](#example_simple_select)
  - [Simple Select 1D](#example_simple_select_1d)
  - [Multiple Select](#example_multiple_select)

<a name="all_supported_fields"></a>

## Supported Fields

```php
 public function __construct(
    public $name,                   // Required: field to put in name="" attribute on input tag
    public $label,                  // Required: Text to show on <label>TEXT</label>
    public ?string $id = null,      // will be populated on the "id" field, will use "name" param if empty
    public $type = "text",          // Type: "text", "textarea", "select", "checkbox", "radio", "file",
    public $current = "",           // Current Value to put in value="" attribute in input tag
    public $required = false,       // Is the Field Required or Not
    public $old = true,             // Show the Old Value (i.e Data returned from old() function
    public $error = true,           // Show Error Message Below
    public $margin = true,          // Add Margin mb-3 or not
    public $autofocus = false,      // Will set autofocus="autofocus" if set true
    public $key = "id",             // SELECT ASSOC Array: key column to put in <option value="KEY">
    public $value = "name",         // SELECT ASSOC Array: value to put inside <option>VALUE</option>
    public $empty = true,           // SELECT: Show "Choose One..." or not
    public $options = [],           // SELECT: Options Array
    public $multiple = false,       // SELECT: Will set multiple="multiple" attribute on <select>
    public bool $checked = false,   // CHECKBOX: set the default state to checked or not
    public bool $switch = false,    // CHECKBOX: use the bootstrap-5 switch for checkbox
    public int  $rows = 2,          // TEXTAREA: number of rows in textarea
) {
}
```

<a name="installation"></a>

## Installation & Update

Create a component using Laravel Artisan.

```bash
php artisan make:component Input
```

Copy content of `Input.php` to `app/View/Components/Input.php`

Copy contents of `input.blade.php` to `resources/views/components/input.blade.php`

Create New File in `resources/views/components/` named `required.blade.php` and copy contents of `required.blade.php` to there.

That's it.

### Update

Copy the updated files (`input.blade.php`, `required.blade.php` and `Input.php` on above mentioned location)

<a name="examples"></a>

## Examples

<a name="example_simple_text"></a>

### Simple Text Input (Create Form)

```html
<x-input field="name" label="Full Name" />
```

<a name="example_simple_text_edit"></a>

### Simple Text Input (Edit Form)

```html
<x-input name="name" label="Full Name" :current="$user->name" />
```

<a name="example_simple_text_required"></a>

### Simple Text Input (Required)

```html
<x-input name="name" label="Full Name" :required="true" />
```

<a name="example_simple_checkbox"></a>

### Simple Checkbox

```html
<x-input name="is_active" label="Is Active" :checked="true" />
```

<a name="example_simple_checkbox_switch"></a>

### Simple Checkbox Switch

```html
<x-input name="is_active" label="Is Active" :checked="true" :switch="true" />
```

<a name="example_simple_select"></a>

### Simple Select

```php
// Controller
$users = User::select(['id', 'name'])->get();
```

```html
<x-input name="user_id" label="Choose User" type="select" :options="$users" />
```

<a name="example_simple_select_1d"></a>

### Select (1D Array)

```php
// Controller
$colors = ['Red', 'Green', 'Blue'];
```

```html
<x-input name="color" label="Choose a Color" :options="$colors" key="" />
```

> Note: For 1-Dimension Array (With Regular Array Index 0, 1, 2...), set `key=""` (empty)

<a name="example_multiple_select"></a>

### Multiple Select

```php
// Controller
$users = User::select(['id', 'name'])->get();
```

```html
<x-input
  name="users[]"
  label="Choose Users"
  type="select"
  :options="$users"
  :multiple="true"
/>
```

## About & License

Author: [Aadarsha](https://aadarsha.com)

Licensed under [MIT License](https://gitlab.com/A4D4RSHA/laravel-x-input-component/-/blob/master/LICENSE.md)
